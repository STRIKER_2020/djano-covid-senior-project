from django.urls import path
from covid import views

urlpatterns = [
    path('', views.index, name = 'home'),
    path('predict', views.predict, name='predict'),
    path('saveresult', views.saveResult, name="saveresult"),
    path('confirm', views.confirm, name='confirm'),
    path('assessment', views.assessment, name='assessment'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register, name='register'),
    path('profile', views.profile, name="profile"),
    path('profile/edit', views.edit_profile, name="edit_profile"),
    path('profile/password', views.change_password, name="change_password"),
    path('maps', views.maps, name='maps'),
]
