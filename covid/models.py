from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.
class UserInfo(models.Model):
    RISK_LEVEL = (
        ('1', 'เสี่ยงระดับ 1'),
        ('2', 'เสี่ยงระดับ 2'),
        ('3', 'เสี่ยงระดับ 3'),
    )

    code = models.IntegerField(verbose_name="User Code", unique=1)
    fname = models.CharField(verbose_name="firstname", max_length=255)
    lname = models.CharField(verbose_name="lastname", max_length=255)
    email = models.EmailField(verbose_name="E-Mail", max_length=254)
    phone = models.CharField(max_length=10)
    risk_lv = models.CharField("Risk Level", max_length=20, choices=RISK_LEVEL, default=RISK_LEVEL[0][1])

    class Meta:
        verbose_name_plural = "User Information"
    
    def __str__(self):
        return self.fname
    

class TypePredict(models.Model):
    p_name = models.CharField(verbose_name="predict name", max_length=200)
    created = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Type of Prediction Result"
    
    def __str__(self):
        return self.p_name
    
class UserHistory(models.Model):
    u_id = models.CharField(verbose_name="USER ID", max_length=10)
    prediction_result = models.CharField(verbose_name="result of prediction", max_length=10)
    created = models.DateTimeField(auto_now=True, auto_now_add=False)
    
    class Meta:
        verbose_name_plural = "User History"
    
    # def ShowUserName(self):
    #     if self.u_id:
    #         user = User.objects.get(id=u_id)
    #         return user.username
    #     return '--'

    def __str__(self):
        return self.prediction_result
    