# Generated by Django 3.1.6 on 2021-03-21 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('covid', '0004_auto_20210322_0024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userhistory',
            name='prediction_result',
            field=models.CharField(max_length=10, verbose_name='result of prediction'),
        ),
    ]
