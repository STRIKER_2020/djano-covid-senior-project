from django.contrib import admin
from django.contrib.auth.models import User
from .models import *

# Register your models here.
class UserHistoryAdmin(admin.ModelAdmin):
    list_display = ['u_id', 'prediction_result', 'created']
    list_filter = ['prediction_result']
    date_hierarchy = 'created'

class MyUserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'last_login']
    list_filter = ['is_active']
    date_hierarchy = 'date_joined'


#   ยกเลิกการผูก Users ในหน้า Django Administration
admin.site.unregister(User)
#  ผูก Users เข้ากับหน้า Django Administration พร้อมกับการปรับแต่ง
admin.site.register(User, MyUserAdmin)

admin.site.register(TypePredict)
admin.site.register(UserHistory, UserHistoryAdmin)