from django.shortcuts import render, redirect, HttpResponseRedirect
from covid.forms import RegisterForm, EditProfileForm, UserLoginFormTH
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserChangeForm
from django.contrib.auth.decorators import login_required
# from sklearn import tree
# from sklearn.model_selection import train_test_split
# import pandas as pd
import requests as rq
from datetime import datetime, date
from geopy.geocoders import Nominatim
from .models import *
from .forms import UserHistoryForm
from django.contrib.auth.models import User
from django.contrib import messages

# from sklearn.svm import SVC

# Create your views here.
def index(request):
    # r = rq.get('https://covid19.th-stat.com/json/covid19v2/getTodayCases.json')
    # print("[HTTP Status of COVD-19 API]: {}".format(r.status_code))
    # print('[USER LOGIN] ',request.user.is_authenticated)
    # print('[USER ID]',request.user.id)
    # if r.status_code == 200:
    #     data = r.json()
    #     dt = datetime.strptime(data['UpdateDate'], "%d/%m/%Y %H:%M").date() #  datetime = "xx/xx/xxxx xx:xx" + date() = "xx/xx/xxxx"
    
    #     context = {
    #         # sent each data from API to templates
    #         'confirmedCase':data['Confirmed'],
    #         'recoveredCase':data['Recovered'],
    #         'hospitalizedCase':data['Hospitalized'],
    #         'deathCase':data['Deaths'],
    #         'newConfirmed':data["NewConfirmed"],
    #         'newRecovered':data["NewRecovered"],
    #         'newHospitalized':data["NewHospitalized"],
    #         'newDeaths':data["NewDeaths"],
    #         'updateDate': dt,
    #         'updateDate2': data['UpdateDate']
    #     }
    # # 404 etc.
    # else:
    #     context = {
    #         # sent each data from API to templates
    #         'confirmedCase': "__",
    #         'recoveredCase': "__",
    #         'hospitalizedCase': "__",
    #         'deathCase': "__",
    #         'newConfirmed': "__",
    #         'newRecovered': "__",
    #         'newHospitalized': "__",
    #         'newDeaths': "__",
    #         'updateDate': "__",
    #     }
    context = {
        # sent each data from API to templates
        'confirmedCase': "__",
        'recoveredCase': "__",
        'hospitalizedCase': "__",
        'deathCase': "__",
        'newConfirmed': "__",
        'newRecovered': "__",
        'newHospitalized': "__",
        'newDeaths': "__",
        'updateDate': "__",
        }
    return render(request, 'covid/index.html', context)

@login_required
def predict(request):
    # dataset
    # dataset_covid = pd.read_csv('https://raw.githubusercontent.com/Nattawut-M/my_csv/main/DATASET_v4.csv')
    # # select attribute 
    # x = dataset_covid[['a1','a2','a3','a4','a5','a6','a7','a8','a9','a10','a11','a12','a13','a14','a15','a16','a17','a18','a19','a20']]
    # # selected label/target
    # y = dataset_covid['result'] 
    # # processing 
    # train_x, test_x, train_y, test_y = train_test_split(x, y, train_size = 0.8)
    # tree_model = tree.DecisionTreeClassifier()
    # tree_model = tree_model.fit(train_x, train_y)
    # tree_score = tree_model.score(test_x, test_y)


    # if submit form 'POST' 
    if request.method == 'POST':
        form = UserHistoryForm()
        ans = []
        userLocation = []
        for i, value in request.POST.items():
            # seperate 'csrf_token', 'latitude', 'longitude' (only data question)
            print('[index] {}, Ans: {}'.format(i, value))
            if i != 'csrfmiddlewaretoken' and i != 'latitude' and i != 'longitude':
                ans.append(value)

        # assign address to variable
        if request.POST['latitude'] and request.POST['longitude']:
            userLocation = [request.POST.get('latitude'), request.POST.get('longitude')]
            # geopy
            geomap = Nominatim(user_agent="MapInfo")
            geolocation = geomap.reverse("{},{}".format(userLocation[0], userLocation[1])) # search
            geoAddress = geolocation.raw['address'] # assign value of raw['address] to variable 'geoAddress' then assign to each variable(area, district, province, country)
            area = geoAddress.get('town','-')
            district = geoAddress.get('county', '-')
            province = geoAddress.get('state', '-')
            country = geoAddress.get('country', '-')
        else:
            # not found latitude and longitude
            area = '-'
            district = '-'
            province = '-'
            country = '-'

        

        print('[Question Answer]:  {}'.format(ans))
        print('[LOCATION]:  {}'.format(userLocation))

        # PREDICTION VALUES
        # result = tree_model.predict([ans])
        # print('[RESULT]\n- type:{} ({})% type: {}'.format(result, (tree_score*100), type(result)))
        # result = result.tolist()
        # # ans_result = result.pop()
        # # print('[result.pop()]: {}'.format(result.pop()))
        # print('[Type of \'toList()\'] : \t{0}'.format(result.pop()))

        # logic tree prediction from rapidminer studio
        if request.POST['question19'] == '1':
            ans_result = 1
            print('question:19 = 1')
        else:
            if request.POST['question18'] == '1':
                ans_result = 1
                print('question:18 = 1')
            else:
                if request.POST['question20'] == '1':
                    if request.POST['question2'] == '1':
                        ans_result = 1
                        print('question:20 & 2 = 1')
                    else:
                        if  request.POST['question11'] == '1':
                            ans_result = 1
                            print('question:11 = 1')
                        else:
                            if request.POST['question4'] == '1':
                                ans_result = 1
                                print('question:4 = 1')
                            else:
                                ans_result = 0
                                print('question:4 = 0')
                else:
                    if  request.POST['question1'] == '1':
                        ans_result = 1
                        print('question:1 = 1')
                    else:
                        if  request.POST['question5'] == '1':
                            if  request.POST['question2'] == '1':
                                ans_result = 1
                                print('question:5 & 2 = 1')
                            else:
                                ans_result = 0
                                print('question:2 = 0')
                        else:
                            ans_result = 0
                            print('question:5 = 0')

        
    riskType = [
        1*(100/3),
        2*(100/3),
        3*(100/3)
    ]
    geomap = [
        area,
        district,
        province,
        country
    ]
    context =  {
        'result': ans_result,
        # 'score': tree_score,
        # 'score': svc_score,
        'userLocation': userLocation,
        'riskType': riskType,
        'geomap':geomap,
        'form':form
    }
    return render(request, 'covid/predict1.html', context)

def saveResult(request):
    if request.method == 'POST':
        print(request.POST)
        form = UserHistoryForm(data=request.POST)
        if form.is_valid():
            form.save(commit=True)
            print('\n\n\n[COMMIT Form] :\t SUCCESSFULLY\n\n\n')
            messages.success(request, "บันทึกผลสำเร็จ")
            return redirect('covid:home')
        else: 
            print('\n\n\n*****[error]*****')
            print(f'[form.is_valid()]:\t{form.is_valid()}\n\n\n')

    return redirect('covid:home')

@login_required
def assessment(request):
    return render(request, 'covid/form_assessment.html')


def confirm(request):
    return render(request, 'covid/confirm.html')


def register(request):
    if request.method == 'POST':

        # check data from POST method
        for i, j in enumerate(request.POST):
            print(i,j)
        
        # change username from Case-Sensitive to lowercase 
        # set mutable before using lowercase 'lower()'
        # request.POST and request.GET will be immutable when accessed in a normal request/response cycle.
        # [FIXED-1] https://stackoverflow.com/questions/44717442/this-querydict-instance-is-immutable
        # [EXPLAIN-1] https://stackoverflow.com/questions/12611345/django-why-is-the-request-post-object-immutable
        print(request.POST['username']) # check before lowercase
        username_case = request.POST['username']
        request.POST._mutable = True # mutable = ค่าเปลี่ยนแปลงได้
        request.POST['username'] = username_case.lower() # set username to lowercase using method 'lower()'
        request.POST._mutable = False
        print(request.POST['username']) # check after lowercase

        # register User 
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save(commit=1)
            messages.success(request, "สมัครสมาชิกสำเร็จ โปรดเข้าสู่ระบบเพื่อใช้งาน")
            request.session['msg_type'] = "success"
            return redirect('covid:login')
    else:
        form = RegisterForm()
    return render(request, 'covid/register.html', {'form':form})


def login_view(request):
    if request.method == 'POST':
        print("[request.POST]\t {}; with Loop {}".format(request.POST, len(request.POST)))
        for i in request.POST:
            print(i)
        loginForm = UserLoginFormTH(data=request.POST)
        next_url = request.GET.get('next') # save page want to visit, but has not login
        print(next_url)
        if loginForm.is_valid():
            user = loginForm.get_user()
            request.session['has_visit'] = False # default 'visit profile' page
            # request.session.set_expiry(600) # set expiry time of session second (60 millisecond = 1 minute)
            login(request, user)
            print(f'[USER]:\t{user}')
            print(f'[USER ID]:\t{user.id}')
            print(f'[USER Firstname]:\t{user.first_name}')
            print(f'[USER Username]:\t{user.username}')
            print(f'[USER E-Mail]:\t{user.email}')

            # check previous page before login, if login success will redirect to continue page before.
            if next_url:
                return HttpResponseRedirect(next_url)
            else :
                return redirect('covid:home')
        else:
            messages.error(request, "กรุณากรอกชื่อผู้ใช้และรหัสผ่านให้ถูกต้อง")
            messages.error(request, "โปรดตรวจสอบความถูกต้องอักษรพิมพ์เล็กและพิมพ์ใหญ่")
            request.session['msg_type'] = 'danger'
            return redirect('covid:login')
    else:
        # ถ้าผู้ใช้เข้าสู่ระบบสำเร็จแล้ว ให้ redirect ไปที่หน้าโปรไฟล์ แทนที่จะเข้าไปที่หน้า login อีกครั้ง
        if request.user.is_authenticated:
            return redirect("covid:profile")
        else:
            loginForm = UserLoginFormTH()
    return render(request, 'covid/login.html', {'loginForm': loginForm})


def logout_view(request):
    if request.method == "POST":
        messages.success(request, "ออกจากระบบสำเร็จ")
        logout(request)
    return redirect('covid:home')


@login_required
def profile(request):
    has_visit = request.session.get('has_visit', False)  # get values of session['has_visit'] if not session['has_visit'](not login) then set default value = false
    # if True = you has visited on your profile pages, then 'messages' for greeting is not necessary
    if request.method == 'GET' and request.user.is_authenticated:
        # request.user.is_authenticated = check user has login , request.user.id = id of user
        user_history = UserHistory.objects.filter(u_id=request.user.id).order_by('-created')
        print('[user_history]:{}'.format(user_history))
        if has_visit:
            msg_greeting = False 
        else:
            request.session['has_visit'] = True
            msg_greeting = True

    context = {
        'msg': msg_greeting,
        'user_history': user_history
    }
    return render(request, 'profile.html', context)

    
def edit_profile(request):

    if  request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, "บันทึกข้อมูลสำเร็จ")
            return redirect('/profile')
    else :
        form = EditProfileForm(instance=request.user)

    context = {
        'request': request,
        'form': form
    }
    return render(request, 'edit_profile.html', context)

def change_password(request):
    if request.method == 'POST':
        print('post change_password')
        print(request.user.id)
        old_password = request.POST.get('old_password', '')
        new_password = request.POST.get('new_password', '')
        old_password = "".join(old_password.split())
        new_password = "".join(new_password.split())
        confirm_password = request.POST.get('confirm_password', '')
        
        print('[old password]: {}'.format(old_password))
        print('[new password]: {}'.format(new_password))
        print('[confirm password]: {}'.format(confirm_password))
        
        #   เช็คว่า user นี้มีรหัสผ่านตรงกับ 'old_password' หรือไม่ (django User Model Methods) ถ้าตรง return = True
        if request.user.check_password(old_password):
            print('check password is True')
            #   ถ้าหากรหัสผ่านใหม่ หรือยืนยันรหัสผ่านใหม่ (อย่างใดอย่างหนึ่ง) น้อยกว่า 8 ตัวอักษร ให้แสดงข้อความประเภท warning แล้ว redirect 
            if len(new_password) < 8 or len(confirm_password) < 8:
                messages.warning(request, "รหัสผ่านใหม่ต้องไม่น้อยกว่า 8 ตัวอักษร")
                return redirect('covid:change_password')
            #   ถ้าหากรหัสผ่านใหม่ "และ" ยืนยันรหัสผ่านใหม่ "ไม่น้อยกว่า" 8 ตัวอักษร
            else:
                #   ถ้าหากรหัสผ่านใหม่ เท่ากับ ยืนยันรหัสผ่านใหม่
                if new_password == confirm_password:
                    print('password validate changed')
                    request.user.set_password(new_password)
                    request.user.save()
                    messages.success(request, "เปลี่ยนรหัสผ่านสำเร็จ โปรดเข้าสู่ระบบใหม่อีกครั้ง")
                    return redirect('covid:login')
                #   รหัสผ่านใหม่ และยืนยันรหัสผ่านใหม่ "ไม่เท่า/ไม่ตรงกัน"
                else:
                    print('2 password not equal')
                    messages.warning(request, "รหัสผ่านใหม่และยืนยันรหัสผ่านใหม่ไม่ตรงกัน")
                    return redirect('covid:change_password')
        #   ถ้าหากไม่ตรง return = False
        else:
            messages.error(request, "รหัสผ่านเดิมผิด")
            print('check password is False')
            return redirect('covid:change_password')

    return render(request, 'change_password.html')


@login_required
def maps(request):
    return render(request, 'maps/maps.html')