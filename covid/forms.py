from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from django import forms
from django.forms import ModelForm
from covid.models import UserHistory
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy 

class UserLoginFormTH(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginFormTH, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'
class RegisterForm(UserCreationForm):
    first_name = forms.CharField(
        label= mark_safe ( 
            gettext_lazy (
                '''<label class="requiredField">ชื่อจริง</label>'''
            )
        ),
        widget=forms.TextInput(
            attrs={
                'placeholder':'สมพงษ์',
                'class':'form-control form-control-sm'
            }
        ),
        max_length=255, 
        required=True
    )
    last_name = forms.CharField(
        label= mark_safe ( 
            gettext_lazy (
                '''<label class="requiredField">นามสกุล</label>'''
            )
        ),
        widget=forms.TextInput(
            attrs={
                'placeholder':'คงกระพัน'
            }
        ),
        max_length=255, 
        required=True
    )
    email = forms.EmailField(
         label= mark_safe ( 
            gettext_lazy (
                '''<label class="requiredField">อีเมล</label>'''
            )
        ),
        widget=forms.EmailInput(
            attrs={
                'placeholder':'xxxx@mail.com'
            }
        ),
        max_length=255, 
        required=True
    )
    username = forms.CharField( 
        label= mark_safe ( 
            gettext_lazy (
                '''<label class="requiredField">ชื่อผู้ใช้</label>'''
            )
        ),
        widget=forms.TextInput(
            attrs={
                'placeholder':'myusername'
            }
        ),
        required=True
    )
    password1 = forms.CharField(
        label= mark_safe ( 
            gettext_lazy (
                '''<label class="requiredField">รหัสผ่าน</label>'''
            )
        ),
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'••••••••••'
            }
        ),
        help_text = mark_safe( gettext_lazy(
            '''<small class="form-text text-muted px-2">
                <li>รหัสผ่านจะต้องไม่ใกล้เคียงกับข้อมูลของคุณ เช่น ชื่อผู้ใช้, ชื่อจริง</li>
                <li>รหัสผ่านจะต้องไม่ต่ำกว่า 8 ตัวอักษร</li>
            </small>'''
        ))
        # help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label= mark_safe( gettext_lazy(
            '''
                <label for="id_password2" class=" requiredField">ยืนยันรหัสผ่าน</label>
            '''
        )),
        widget=forms.PasswordInput(
            attrs={'placeholder':"••••••••••"}
        ),
        help_text= mark_safe( gettext_lazy(
            """
            <small class="form-text text-muted px-2">
                <li>โปรดตรวจสอบให้แน่ใจว่ารหัสผ่านนั้นตรงกัน</li>
            </small>
            """
        ) )
    )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password1', 'password2']

    def __init__ (self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        # self.fields['first_name'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['last_name'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['email'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['username'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['password1'].widget.attrs['class'] = 'form-control form-control-sm'
        self.fields['password2'].widget.attrs['class'] = 'form-control form-control-sm'
        #   แจ้งเตือนเมื่อรหัสผ่าน 1 และยืนยันรหัสผ่าน ไม่ตรงกัน จะส่งข้อความนี้กลับไป
        self.error_messages['password_mismatch'] = "รหัสผ่านไม่ตรงกัน"

class EditProfileForm(UserChangeForm):
    first_name = forms.CharField(label="ชื่อจริง")
    last_name = forms.CharField(label="นามสกุล")
    email = forms.CharField(label="อีเมล")
    password = forms.CharField(
        label="รหัสผ่านแบบเข้ารหัส",
        widget=forms.HiddenInput()
    )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']

    def __init__ (self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['class'] = 'form-control form-control'
        self.fields['last_name'].widget.attrs['class'] = 'form-control form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control form-control disable'
        # self.fields['password1'].widget.attrs['class'] = 'form-control form-control-sm'
        # self.fields['password2'].widget.attrs['class'] = 'form-control form-control-sm'
      
class UserHistoryForm(ModelForm):
    class Meta:
        model = UserHistory
        fields = '__all__'
