from django.shortcuts import render
# import Paginator
from django.core.paginator import Paginator, InvalidPage, PageNotAnInteger, EmptyPage
from news.models import *
# Create your views here.
def home(request):
    news = News.objects.all().order_by('-created')
    type_news = TypeNews.objects.all()

    news_paginator = Paginator(news, 6) # show news 6 items per page
    page_index = request.GET.get('page') # get page you want to access
    print("[page_index] {}".format(page_index))
    try:
        if page_index:
            news = news_paginator.page(page_index)
        else:
            news = news_paginator.page(1)
    except PageNotAnInteger:
        news = news_paginator.page(1)
    except EmptyPage:
        # if page_index over than 'number of news_paginator' then return to 'last of news_paginator' instead
        news = news_paginator.page(news_paginator.num_pages)
    except InvalidPage:
        news = news_paginator.page(1)

    print("[news.number]:\t{}".format(news.number)) # show number of current page

    return render(request, 'news/news.html', {'news':news, 'type_news':type_news})

def news_detail(request, slug):
    news = News.objects.get(slug=slug)
    current_page = request.GET.get('p')

    return render(request, 'news/news_detail.html', {'news':news, 'current':current_page})
    
def news_category(request, id):
    news_cat = TypeNews.objects.get(id=id)
    news = News.objects.filter(type_news=id)

    return render(request, 'news/news.html', {'news_cat':news_cat, 'news':news})

def news_category_detail(request, id, slug):
    news = News.objects.filter(type_news=id, slug=slug)

    return render(request, 'news/news.html', {'news':news})