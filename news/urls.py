from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('detail/<slug:slug>', views.news_detail, name="detail"),
    path('categories/<int:id>/', views.news_category, name="category"),
    path('categories/<int:id>/<slug:slug>', views.news_category_detail, name="category_detail"),
]
