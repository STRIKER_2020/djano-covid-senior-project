from django.db import models
from django.utils import timezone
from django.utils.html import format_html

# Create your models here.
class TypeNews(models.Model):
    type_news = models.CharField("Type of News", max_length=100)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Type of News"
        
    def __str__(self):
        return self.type_news
    

class News(models.Model):
    code = models.IntegerField("Code of News", unique=True)
    name = models.CharField("Name of News", max_length=255, blank=False, null=False)
    slug = models.SlugField(max_length=255)
    img = models.ImageField(upload_to="imgNews", verbose_name="Images News", null=True, blank=True)
    title = models.TextField("Head Title")
    imgC1 = models.ImageField(upload_to="imgNews", null=True, blank=True)
    content1 = models.TextField(blank=True, null=True)
    imgC2 = models.ImageField(upload_to="imgNews", null=True, blank=True)
    content2 = models.TextField(blank=True, null=True)
    imgC3 = models.ImageField(upload_to="imgNews", null=True, blank=True)
    content3 = models.TextField(blank=True, null=True)
    imgC4 = models.ImageField(upload_to="imgNews", null=True, blank=True)
    content4 = models.TextField(blank=True, null=True)
    type_news = models.ManyToManyField(TypeNews, verbose_name="Type of News", blank=False)
    published = models.BooleanField(blank=False, null=False, default=1)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "News"

    def __str__(self):
        return self.name
    
    def show_img(self):
        if self.img :
            return format_html('<img src={} height="40px">'.format(self.img.url))
        return ''
    show_img.allow_tags = True
    show_img.short_description = "img of news"