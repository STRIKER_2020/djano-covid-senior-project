from django.contrib import admin
from .models import *

# Register your models here.
class NewsAdmin(admin.ModelAdmin):
    list_display = ['code', 'name', 'created', 'published', 'show_img']
    list_filter = ['type_news', 'published']
    search_fields = ['name', 'title']
    date_hierarchy = 'created'
    prepopulated_fields = {'slug': ['code', 'name', 'title', 'type_news']}

class TypeNewsAdmin(admin.ModelAdmin):
    list_display = ['type_news', 'created']
    search_fields = ['type_news']
    date_hierarchy = 'created'

admin.site.register(TypeNews, TypeNewsAdmin)
admin.site.register(News, NewsAdmin)